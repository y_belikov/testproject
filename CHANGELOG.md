Решение тестового задания опишу в виде следующих шагов

1. После клонирования репозитория и выполнения предварительной инструкции присланной коммандой BaseCamp`a 
   попытался сбилдить проект при помощи make.
2. Просмотрел лог ошибок сборки. Добавил "заглушки" для объявленных изначально, но не реализованных методов

3. Снова забилдил. Сборка произошла успешно, ни один тест не прошел

4. Открыл файл с набором тестов и попытался понять какое поведение ожидается от объекта класса Generator

Исходя из тестов сделал следующее: 

5. Удалил директивы pragma_pack в конце и в начале объявления класса, чтобы избавиться от нестандартного выравнивания полей класса. 

6. Реализовал метод set класса Generator, конструктор по умолчанию, оператор присваивания. Для улучшения читаемости кода и чтобы избежать дублирований
   реализовал методы init и copy, которые отвечают за инициализацию членов-данных класса. В метод set добавил проверку невалидности передаваемых аргументов:
   в случае ошибки, кидается logic_error исключение.

7. Исходя из тестов попытался выяснить поведение функции check и то, как должна выглядеть генерируемая последовательность в зависимости от своей длинны

8. Создал кастомный тест в котором проходил все целые числа в определенном диапазоне, вызывая для каждого метод check. Проверял на каких числах check возвращает true

9. Манипуляцией описанной в шаге 8 выяснил что последовательность может состоять из 32 парных чисел, 24 квадратов целых чисел, начиная с 8, 392 простых чисел
   от 1031 до 4094. В случае если последовательность должна быть больше чем 448, заполнить до конца "магическим" числом 3111696, что равно 42^4.

10. Реализовал фукнцию генерации путем вызовов четырех вспомогательных функций, отвечающих за генерацию каждой части подпоследовательности (см. шаг 9)
    Три фукнции получают в качестве аргументов индекс начала подпослдедовательности, индекс ее конца, и начальное значение. Функция, заполняющая остаток 
    последовательности "магическим" числом, получает только индекс начала. В начале тела каждой функции выполняется проверка на то, что начальный индекс
    должен быть меньше, чем общая длинна последовательности. В случае не выполнения условия - функция ничего не сгенерирует.

11. Для ускорения генерации простых чисел использовал решето Эратосфена.

12. Перезапустил тесты - все прошли. Закомитил решение в удаленную репу предоставленную командой BaseCamp`a. Запустил тесты на Jenkins, получил синий кружок
    означающий успешное выполнение все тестов

13. Добавил удаленный GitLab репозиторий

14. Создал отдельную ветку intro/solution и опубликовал ее в GitLab

15. Установил clang в качестве пакета Cygwin.

16. Создал .clang-format файл используя комманду "clang-format -style=google"

17. Изменил файлы в соответствии со стилем указанном в .clang-format используя комманду "clang-format <file_name> -i"

18. Создал коммит "fixup! clang-format"

19. Сделал пуш коммита на удалленную ветку



# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [1.0.0] - 2020-11-06
### Added 
- .gitlab-ci.yml file to build project and run tests on GitLab CI/CD

### Changed
- CHANGELOG.md according to [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)
- Language for file maintainence (from Russian to English)

## [0.2.0] - 2020-11-06
### Added
- CHANGELOG.md to document all changes to this project and with test task solution description 

## [0.1.0] - 2020-11-06
### Added 
- .clang-format based on [Google C++ Style Guide](https://google.github.io/styleguide/cppguide.html)

### Changed
- generator.h according to style defined in .clang-format
- generator.cpp according to style defined in .clang-format

## [0.0.3] - 2020-09-12
## Changed
- In generator.cpp implementation of generate and set methods

## [0.0.2] - 2020-09-07
### Added
- Method to generate subsequence of even numbers as a part of required sequence
- Method to genereate subsequence of squares as a part of required sequence
- Method to genereate subsequence of prime numbers as a part of required sequence
- Method which fills tail of the sequence with "magic" number equal to 42^4

### Changed
- Implementation of method generate in generator.cpp 

## [0.0.1] - 2020-09-06
### Added
- The class method to check validity of fields initialization
- The class method for fields intialization(calls from the constructor)
- The class method to initialize object fields through copy of the same class
- Copy assignment operator of the class
- Implementation of method generate in generator.cpp
- Destructor
- Custom test case to search the parts of the required sequence

### Changed
- Generator class method to set values of fields
- Generator class method for sequence creation 
- Method for generating sequence implementation
- get_data() method implementation

### Removed
- #pragma_pack(PUSH, 1) and #pragma_pack(POP) preprocessor directives in generator.h

[1.0.0]: -//-
[0.2.0]: https://gitlab.com/procamp/20q3/trainees/yurii.bielikov/-/commit/d557f6e7c5e3dff749ae543f025df75f20cf3d84
[0.1.0]: https://gitlab.com/procamp/20q3/trainees/yurii.bielikov/-/commit/7f2ee9753f10793888b998e88bab1758efd2ac63
[0.0.3]: https://gitlab.com/procamp/20q3/trainees/yurii.bielikov/-/commit/c41885754165c9d437ffc9b832d132b17c410908
[0.0.2]: https://gitlab.com/procamp/20q3/trainees/yurii.bielikov/-/commit/c41885754165c9d437ffc9b832d132b17c410908
[0.0.1]: https://gitlab.com/procamp/20q3/trainees/yurii.bielikov/-/commit/2fa6b28a814f85eda00eef82b10a476a69b40676