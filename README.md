The list of files which are allowed to be modified:
    project/generator.cpp
    project/generator.h

Other files will be restored on Jenkins CI side before each build.
