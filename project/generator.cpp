/*
 * generator.cpp
 *
 *  Created for: GlobalLogic + KNU Basecamp
 *       Author: vitalii.lysenko + serhii.denysov
 *
 * Generator class source.
 *
 * You may have to change this file to fix build errors, and make
 * the unit tests pass.
 *
 * Usually source files are used to write an implementation of
 * global and member functions.
 *
 */

#include "generator.h"
#include "check.h"

#include <cstring>
#include <iostream>
#include <limits>
#include <stdexcept>

/*
 * To make all unit tests pass you may try to discover the ::check(int n) logic
 * and create suitable generator. Or you can invent another way.
 *
 * Good luck!
 *
 */

void Generator::init(int *newData, int n) {
  data = new int[n];
  std::copy(newData, newData + n, data);
  count = n;
}

void Generator::copy(Generator &other) {
  delete[] data;
  init(other.get_data(), other.get_count());
}

Generator &Generator::operator=(Generator &other) {
  if (this != &other) {
    copy(other);
  }
  return *this;
}

int *Generator::get_data() const { return data; }

void Generator::set(int *newData, int n) {
  checkConstraints(newData, n);

  delete[] data;
  init(newData, n);
}

void Generator::checkConstraints(int *data, int n) {
  if (!data || (data && !n) || n < 0) {
    throw std::logic_error(
        "Error occured while initializing generators' data!");
  }
}

void Generator::generate(int count) {
  delete[] data;
  generate_values(count);
}

int Generator::get_count() const { return count; }

void Generator::generate_values(int count) {
  data = new int[count];
  this->count = count;

  const int even_numbers_high_boundary = 32;
  const int squares_high_boundary = 56;
  const int prime_numbers_high_boundary = 448;
  const int init_square_value = 8;
  const int init_primes_value = 1031;
  const int highest_primes_value = 4094;
  const int perfect_number_to_fill = 3111696;

  generate_even_numbers(0, even_numbers_high_boundary);
  generate_squares(even_numbers_high_boundary, squares_high_boundary,
                   init_square_value);
  generate_prime_numbers(squares_high_boundary, prime_numbers_high_boundary,
                         init_primes_value, highest_primes_value);
  fiil_tail(prime_numbers_high_boundary, perfect_number_to_fill);
}

void Generator::generate_even_numbers(const int low_boundary,
                                      const int high_boundary) {
  int high = high_boundary > count ? count : high_boundary;
  for (int i = low_boundary; i < high; ++i) {
    data[i] = 2 * i;
  }
}

void Generator::generate_squares(const int low_boundary,
                                 const int high_boundary,
                                 const int init_value) {
  int value = init_value;
  int high = high_boundary > count ? count : high_boundary;
  for (int i = low_boundary; i < high; ++i) {
    data[i] = value * value;
    ++value;
  }
}

void Generator::generate_prime_numbers(const int low_boundary,
                                       const int high_boundary,
                                       const int init_primes_value,
                                       const int highest_primes_value) {
  if (low_boundary >= count)
    return;

  int current_prime = init_primes_value;

  int high = high_boundary > count ? count : high_boundary;

  bool sieve[highest_primes_value];

  for (int i = 0; i < highest_primes_value; ++i) {
    sieve[i] = true;
  }

  for (int i = 2; i * i < highest_primes_value; ++i) {
    if (!sieve[i])
      continue;

    for (int j = i; j < highest_primes_value; j += i) {
      sieve[j] = false;
    }
  }
  for (int i = low_boundary; i < high; ++i) {
    data[i] = current_prime;
    for (int k = current_prime + 1; k < highest_primes_value; ++k) {
      if (sieve[k]) {
        current_prime = k;
        break;
      }
    }
  }
}

void Generator::fiil_tail(const int low_boundary, const int number_to_fill) {
  for (int i = low_boundary; i < count; ++i) {
    data[i] = number_to_fill;
  }
}

Generator::~Generator() { delete[] data; }
