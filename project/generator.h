/*
 * generator.h
 *
 *  Created for: GlobalLogic + KNU Basecamp
 *       Author: vitalii.lysenko + serhii.denysov
 *
 * Generator class header.
 *
 * You have to change this file to fix build errors, and make
 * the unit tests passed.
 *
 */

#ifndef GENERATOR_H
#define GENERATOR_H

/*
 * Generator class header.
 *
 */
class Generator {
private:
  void init(int *newData, int n);
  void copy(Generator &other);

public:
  Generator() : data(0), count(0){};
  Generator &operator=(Generator &other);
  int *get_data() const;
  void set(int *newData, int n);
  int get_count() const;

  void generate(int count);
  ~Generator();

private:
  void generate_values(int count);
  void checkConstraints(int *data, int n);
  void generate_even_numbers(const int low_boundary, const int high_boundary);
  void generate_squares(const int low_boundary, const int high_boundary,
                        const int init_value);
  void generate_prime_numbers(const int low_boundary, const int high_boundary,
                              const int init_primes_value,
                              const int highest_primes_value);
  void fiil_tail(const int low_boundary, const int number_to_fill);

  int *data;
  int count;
};

#endif // GENERATOR_H
