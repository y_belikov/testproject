#ifndef INTRO_CHECK_H
#define INTRO_CHECK_H

/*
 * This function was pre-compiled and is provided as a part of the
 * static library.
 *
 */

bool check(int n);

#endif  // INTRO_CHECK_H
